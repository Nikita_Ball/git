#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
    double weight = 0;
    double gprice = 1850000;
    double yprice = 0;
    printf("Enter your weight \n");
    scanf("%d", &weight);
    if (weight > 0)
    {
        yprice = weight*gprice;
        printf("Your price is %d \n", yprice);
        return 0;
    }
    else
    {
        printf("Wrong value \n");
        return 1;
    }
}